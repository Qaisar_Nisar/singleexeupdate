﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SharpUpdate;
using System.Net.NetworkInformation;

namespace TestUpdate
{
    public partial class Form1 : Form,ISharpUpdatable
    {
        static bool networkIsAvailable = false;  
        public SharpUpdater updater;
        public Form1()
        {

            InitializeComponent();
            //this.label1.Text = this.ApplicationAssemnly.GetName().Version.ToString();

            //Network Checking 
            NetworkInterface[] nics = NetworkInterface.GetAllNetworkInterfaces();


            foreach (NetworkInterface nic in nics)
            {
                if ((nic.NetworkInterfaceType != NetworkInterfaceType.Loopback && nic.NetworkInterfaceType != NetworkInterfaceType.Tunnel) &&
                    nic.OperationalStatus == OperationalStatus.Up)
                {
                    networkIsAvailable = true;
                }
            }

            this.labelVersion.Text = this.ApplicationAssembly.GetName().Version.ToString();
           updater = new SharpUpdater(this);
          // su.DoUpdate();
        }

        public string ApplicationName
        {
            get { return "TestUpdate"; }
        }

        public string ApplicationID
        {
            get { return "TestUpdate"; }
        }

        public System.Reflection.Assembly ApplicationAssembly
        {
            get { return Assembly.GetExecutingAssembly(); }
        }

        public Icon ApplicationIcon
        {
            get { return this.Icon; }
        }

        public Uri UpdateXmlLocation
        {
            get { return new Uri("http://88.208.208.184/buzybeezukdata/temp/update.xml"); }
        }

        public Form Context
        {
            get { return this; }
        }

        private void button1_Click(object sender, EventArgs e)
        {

        Restart:
            NetworkInterface[] nics = NetworkInterface.GetAllNetworkInterfaces();
            foreach (NetworkInterface nic in nics)
            {
                if ((nic.NetworkInterfaceType != NetworkInterfaceType.Loopback && nic.NetworkInterfaceType != NetworkInterfaceType.Tunnel) &&
                    nic.OperationalStatus == OperationalStatus.Up)
                {
                    networkIsAvailable = true;
                }
            }

            if (!networkIsAvailable)
            {
                DialogResult result = MessageBox.Show("Internet connection not available! We resume as soon as network is available...", "Internet Connection Error!", MessageBoxButtons.RetryCancel, MessageBoxIcon.Error);

                if (result == DialogResult.Retry)
                    goto Restart;
            }
            else
                updater.DoUpdate();

        }
    }
}
