﻿using System;
using System.Collections.Generic;
namespace SharpUpdate
{
    internal class SharpUpdateFileInfo
    {
        /// <summary>
        /// Holds the url for download
        /// </summary>
       // private string url;
      //private  List<string> url = new List<string>();
      string url;

        /// <summary>
        /// Holds th filename
        /// </summary>
        private string fileName;

        /// <summary>
        /// Holds the md5 checksum
        /// </summary>
        private string md5;

        /// <summary>
        /// Holds the path to the temporarly downloaded file
        /// </summary>
        private string tempFile;

        /// <summary>
        /// Creates a new SharpUpdateFileInfo object
        /// </summary>
        /// 
        Uri URIurl;
        Version VersionVer;
        internal SharpUpdateFileInfo(string url, string fileName, string md5)
        {
            this.url = url;
            this.fileName = fileName;
            this.md5 = md5;
        }
        internal SharpUpdateFileInfo(Uri url, string fileName, Version ver, string md5)
        {
            this.URIurl = url;
            this.fileName = fileName;
            this.md5 = md5;
            this.VersionVer = ver;
            //FileNameVersion.Add(fileName,version);

            
            

        }

        /// <summary>
        /// The download url for the file
        /// </summary>
        internal string Url
        {
            get { return url; }
            //set { url = value; }
        }

        /// <summary>
        /// The filename
        /// </summary>
        internal string FileName
        {
            get { return fileName; }
            //set { fileName = value; }
        }
        internal Uri URI_Url
        {
            get { return URIurl; }
            //set { url = value; }
        }

        internal Version Version_Ver
        {
            get
            {
                return VersionVer;
            }
        }
        /// <summary>
        /// The Md5-Checksum of the file
        /// </summary>
        internal string Md5
        {
            get { return md5; }
            //set { md5 = value; }
        }

        /// <summary>
        /// Path to the downloaded file
        /// </summary>
        internal string TempFile
        {
            get { return tempFile; }
            set { tempFile = value; }
        }
    }
}
