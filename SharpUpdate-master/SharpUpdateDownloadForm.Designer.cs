﻿namespace SharpUpdate
{
    partial class SharpUpdateDownloadForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblDownloading = new System.Windows.Forms.Label();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.lblProgress = new System.Windows.Forms.Label();
            this.progressBarAll = new System.Windows.Forms.ProgressBar();
            this.Lbl_WaitNetworkDisconnect = new System.Windows.Forms.Label();
            this.btn_DownloadCancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblDownloading
            // 
            this.lblDownloading.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDownloading.Location = new System.Drawing.Point(12, 21);
            this.lblDownloading.Name = "lblDownloading";
            this.lblDownloading.Size = new System.Drawing.Size(389, 45);
            this.lblDownloading.TabIndex = 0;
            this.lblDownloading.Text = "Update is downloading ...";
            this.lblDownloading.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // progressBar
            // 
            this.progressBar.Location = new System.Drawing.Point(34, 83);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(344, 23);
            this.progressBar.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.progressBar.TabIndex = 1;
            // 
            // lblProgress
            // 
            this.lblProgress.Font = new System.Drawing.Font("Segoe UI", 8F);
            this.lblProgress.Location = new System.Drawing.Point(34, 122);
            this.lblProgress.Name = "lblProgress";
            this.lblProgress.Size = new System.Drawing.Size(344, 13);
            this.lblProgress.TabIndex = 2;
            this.lblProgress.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // progressBarAll
            // 
            this.progressBarAll.Location = new System.Drawing.Point(34, 138);
            this.progressBarAll.Name = "progressBarAll";
            this.progressBarAll.Size = new System.Drawing.Size(344, 23);
            this.progressBarAll.TabIndex = 3;
            // 
            // Lbl_WaitNetworkDisconnect
            // 
            this.Lbl_WaitNetworkDisconnect.AutoSize = true;
            this.Lbl_WaitNetworkDisconnect.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.Lbl_WaitNetworkDisconnect.ForeColor = System.Drawing.Color.Red;
            this.Lbl_WaitNetworkDisconnect.Location = new System.Drawing.Point(34, 189);
            this.Lbl_WaitNetworkDisconnect.Name = "Lbl_WaitNetworkDisconnect";
            this.Lbl_WaitNetworkDisconnect.Size = new System.Drawing.Size(222, 15);
            this.Lbl_WaitNetworkDisconnect.TabIndex = 4;
            this.Lbl_WaitNetworkDisconnect.Text = "*Waiting for your internet availability...";
            // 
            // btn_DownloadCancel
            // 
            this.btn_DownloadCancel.Location = new System.Drawing.Point(302, 185);
            this.btn_DownloadCancel.Name = "btn_DownloadCancel";
            this.btn_DownloadCancel.Size = new System.Drawing.Size(75, 23);
            this.btn_DownloadCancel.TabIndex = 5;
            this.btn_DownloadCancel.Text = "Cancel";
            this.btn_DownloadCancel.UseVisualStyleBackColor = true;
            // 
            // SharpUpdateDownloadForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(420, 221);
            this.Controls.Add(this.btn_DownloadCancel);
            this.Controls.Add(this.Lbl_WaitNetworkDisconnect);
            this.Controls.Add(this.progressBarAll);
            this.Controls.Add(this.lblProgress);
            this.Controls.Add(this.progressBar);
            this.Controls.Add(this.lblDownloading);
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SharpUpdateDownloadForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Downloading Update";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.SharpUpdateDownloadForm_FormClosed);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblDownloading;
        private System.Windows.Forms.ProgressBar progressBar;
        private System.Windows.Forms.Label lblProgress;
        private System.Windows.Forms.ProgressBar progressBarAll;
        private System.Windows.Forms.Label Lbl_WaitNetworkDisconnect;
        private System.Windows.Forms.Button btn_DownloadCancel;
    }
}